# XNU-Font

The console font from XNU/Darwin (Apple) kernel for Linux console.

## Screenshot

![Screenshot](screenshot.png)

## Installation

Run `make font` and install the resulting PSF file according to your distro.

